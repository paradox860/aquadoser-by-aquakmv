void Pump_screen (int8_t *p, String p_name ) {
  lcd.setCursor(7, 0);
  lcd.print(p_name);
  lcd.setCursor(0, 1);
  lcd.print("ВРЕМЯ");
  lcd.setCursor(0, 2);
  lcd.print("ДЕНЬ П В С Ч П С В");
      lcd.setCursor(11, 1);
      lcd.print(">>");
  if (enc1.isRight()) {
    //  lcd.clear();
    if (++menu_val_2 >= 10) menu_val_2 = 0;  //обработка поворота энкодера без нажатия - проход по меню
  } else if (enc1.isLeft()) {
    //   lcd.clear();
    if (--menu_val_2 < 0) menu_val_2 = 9; //обработка поворота энкодера без нажатия - проход по меню
  }

  switch (menu_val_2)
  {
    case 0:
      lcd.setCursor(5, 1);
      lcd.write(126);
      lcd.setCursor(6, 1);
      if (p[0] < 10) {
        lcd.print("0");
      }
      lcd.print(p[0]);
      lcd.print(":");
      if (p[1] < 10) {
        lcd.print("0");
      }
      lcd.print(p[1]);
      lcd.setCursor(13, 1);
      lcd.print(" ");
      lcd.setCursor(14, 1);
      lcd.print(p[2]);
      lcd.print(" СЕК");
      lcd.setCursor(4, 3);
      lcd.print(" ");
      lcd.print(p[4]);
      lcd.print(" ");
      lcd.print(p[5]);
      lcd.print(" ");
      lcd.print(p[6]);
      lcd.print(" ");
      lcd.print(p[7]);
      lcd.print(" ");
      lcd.print(p[8]);
      lcd.print(" ");
      lcd.print(p[9]);
      lcd.print(" ");
      lcd.print(p[3]);
      if (enc1.isRightH()) {
        //  lcd.clear();
        if (++p[0] >= 24) p[0] = 0;
      } else if (enc1.isLeftH()) {
        //   lcd.clear();
        if (--p[0] < 0) p[0] = 23;
      }
      break;
    case 1:
      lcd.setCursor(5, 1);
      lcd.print(" ");
      lcd.setCursor(6, 1);
      if (p[0] < 10) {
        lcd.print("0");
      }
      lcd.print(p[0]);
      lcd.write(126);
      if (p[1] < 10) {
        lcd.print("0");
      }
      lcd.print(p[1]);
      lcd.setCursor(13, 1);
      lcd.print(" ");
      lcd.setCursor(14, 1);
      lcd.print(p[2]);
      lcd.print(" СЕК");
      lcd.setCursor(4, 3);
      lcd.print(" ");
      lcd.print(p[4]);
      lcd.print(" ");
      lcd.print(p[5]);
      lcd.print(" ");
      lcd.print(p[6]);
      lcd.print(" ");
      lcd.print(p[7]);
      lcd.print(" ");
      lcd.print(p[8]);
      lcd.print(" ");
      lcd.print(p[9]);
      lcd.print(" ");
      lcd.print(p[3]);
      if (enc1.isRightH()) {
        //  lcd.clear();
        if (++p[1] >= 59) p[1] = 0;
      } else if (enc1.isLeftH()) {
        //   lcd.clear();
        if (--p[1] < 0) p[1] = 60;
      }
      break;

      break;
    case 2:
      lcd.setCursor(5, 1);
      lcd.print(" ");
      lcd.setCursor(6, 1);
      if (p[0] < 10) {
        lcd.print("0");
      }
      lcd.print(p[0]);
      lcd.print(":");
      if (p[1] < 10) {
        lcd.print("0");
      }
      lcd.print(p[1]);
      lcd.setCursor(13, 1);
      lcd.write(126);
      lcd.setCursor(14, 1);
      lcd.print(p[2]);
      lcd.print(" СЕК");
      lcd.setCursor(4, 3);
      lcd.print(" ");
      lcd.print(p[4]);
      lcd.print(" ");
      lcd.print(p[5]);
      lcd.print(" ");
      lcd.print(p[6]);
      lcd.print(" ");
      lcd.print(p[7]);
      lcd.print(" ");
      lcd.print(p[8]);
      lcd.print(" ");
      lcd.print(p[9]);
      lcd.print(" ");
      lcd.print(p[3]);
      if (enc1.isRightH()) {
        //  lcd.clear();
        if (++p[2] >= PERIOD+1) p[2] = 0;
      } else if (enc1.isLeftH()) {
        //   lcd.clear();
        if (--p[2] < 0) p[2] = PERIOD;
      }
      break;
    case 3:
      lcd.setCursor(5, 1);
      lcd.print(" ");
      lcd.setCursor(6, 1);
      if (p[0] < 10) {
        lcd.print("0");
      }
      lcd.print(p[0]);
      lcd.print(":");
      if (p[1] < 10) {
        lcd.print("0");
      }
      lcd.print(p[1]);
      lcd.setCursor(13, 1);
      lcd.print(" ");
      lcd.setCursor(14, 1);
      lcd.print(p[2]);
      lcd.print(" СЕК");
      lcd.setCursor(4, 3);
      lcd.write(126);
      lcd.print(p[4]);
      lcd.print(" ");
      lcd.print(p[5]);
      lcd.print(" ");
      lcd.print(p[6]);
      lcd.print(" ");
      lcd.print(p[7]);
      lcd.print(" ");
      lcd.print(p[8]);
      lcd.print(" ");
      lcd.print(p[9]);
      lcd.print(" ");
      lcd.print(p[3]);
      if (enc1.isRightH()) {
        //  lcd.clear();
        if (++p[4] >= 2) p[4] = 0;
      } else if (enc1.isLeftH()) {
        //   lcd.clear();
        if (--p[4] < 0) p[4] = 1;
      }

      break;
    case 4:
      lcd.setCursor(5, 1);
      lcd.print(" ");
      lcd.setCursor(6, 1);
      if (p[0] < 10) {
        lcd.print("0");
      }
      lcd.print(p[0]);
      lcd.print(":");
      if (p[1] < 10) {
        lcd.print("0");
      }
      lcd.print(p[1]);
      lcd.setCursor(13, 1);
      lcd.print(" ");
      lcd.setCursor(14, 1);
      lcd.print(p[2]);
      lcd.print(" СЕК");
      lcd.setCursor(4, 3);
      lcd.print(" ");
      lcd.print(p[4]);
      lcd.write(126);
      lcd.print(p[5]);
      lcd.print(" ");
      lcd.print(p[6]);
      lcd.print(" ");
      lcd.print(p[7]);
      lcd.print(" ");
      lcd.print(p[8]);
      lcd.print(" ");
      lcd.print(p[9]);
      lcd.print(" ");
      lcd.print(p[3]);
      if (enc1.isRightH()) {
        //  lcd.clear();
        if (++p[5] >= 2) p[5] = 0;
      } else if (enc1.isLeftH()) {
        //   lcd.clear();
        if (--p[5] < 0) p[5] = 1;
      }
      break;
    case 5:
      lcd.setCursor(5, 1);
      lcd.print(" ");
      lcd.setCursor(6, 1);
      if (p[0] < 10) {
        lcd.print("0");
      }
      lcd.print(p[0]);
      lcd.print(":");
      if (p[1] < 10) {
        lcd.print("0");
      }
      lcd.print(p[1]);
      lcd.setCursor(13, 1);
      lcd.print(" ");
      lcd.setCursor(14, 1);
      lcd.print(p[2]);
      lcd.print(" СЕК");
      lcd.setCursor(4, 3);
      lcd.print(" ");
      lcd.print(p[4]);
      lcd.print(" ");
      lcd.print(p[5]);
      lcd.write(126);
      lcd.print(p[6]);
      lcd.print(" ");
      lcd.print(p[7]);
      lcd.print(" ");
      lcd.print(p[8]);
      lcd.print(" ");
      lcd.print(p[9]);
      lcd.print(" ");
      lcd.print(p[3]);
      if (enc1.isRightH()) {
        //  lcd.clear();
        if (++p[6] >= 2) p[6] = 0;
      } else if (enc1.isLeftH()) {
        //   lcd.clear();
        if (--p[6] < 0) p[6] = 1;
      }

      break;
    case 6:
      lcd.setCursor(5, 1);
      lcd.print(" ");
      lcd.setCursor(6, 1);
      if (p[0] < 10) {
        lcd.print("0");
      }
      lcd.print(p[0]);
      lcd.print(":");
      if (p[1] < 10) {
        lcd.print("0");
      }
      lcd.print(p[1]);
      lcd.setCursor(13, 1);
      lcd.print(" ");
      lcd.setCursor(14, 1);
      lcd.print(p[2]);
      lcd.print(" СЕК");
      lcd.setCursor(4, 3);
      lcd.print(" ");
      lcd.print(p[4]);
      lcd.print(" ");
      lcd.print(p[5]);
      lcd.print(" ");
      lcd.print(p[6]);
      lcd.write(126);
      lcd.print(p[7]);
      lcd.print(" ");
      lcd.print(p[8]);
      lcd.print(" ");
      lcd.print(p[9]);
      lcd.print(" ");
      lcd.print(p[3]);
      if (enc1.isRightH()) {
        //  lcd.clear();
        if (++p[7] >= 2) p[7] = 0;
      } else if (enc1.isLeftH()) {
        //   lcd.clear();
        if (--p[7] < 0) p[7] = 1;
      }

      break;
    case 7:
      lcd.setCursor(5, 1);
      lcd.print(" ");
      lcd.setCursor(6, 1);
      if (p[0] < 10) {
        lcd.print("0");
      }
      lcd.print(p[0]);
      lcd.print(":");
      if (p[1] < 10) {
        lcd.print("0");
      }
      lcd.print(p[1]);
      lcd.setCursor(13, 1);
      lcd.print(" ");
      lcd.setCursor(14, 1);
      lcd.print(p[2]);
      lcd.print(" СЕК");
      lcd.setCursor(4, 3);
      lcd.print(" ");
      lcd.print(p[4]);
      lcd.print(" ");
      lcd.print(p[5]);
      lcd.print(" ");
      lcd.print(p[6]);
      lcd.print(" ");
      lcd.print(p[7]);
      lcd.write(126);
      lcd.print(p[8]);
      lcd.print(" ");
      lcd.print(p[9]);
      lcd.print(" ");
      lcd.print(p[3]);
      if (enc1.isRightH()) {
        //  lcd.clear();
        if (++p[8] >= 2) p[8] = 0;
      } else if (enc1.isLeftH()) {
        //   lcd.clear();
        if (--p[8] < 0) p[8] = 1;
      }
      break;
    case 8:
      lcd.setCursor(5, 1);
      lcd.print(" ");
      lcd.setCursor(6, 1);
      if (p[0] < 10) {
        lcd.print("0");
      }
      lcd.print(p[0]);
      lcd.print(":");
      if (p[1] < 10) {
        lcd.print("0");
      }
      lcd.print(p[1]);
      lcd.setCursor(13, 1);
      lcd.print(" ");
      lcd.setCursor(14, 1);
      lcd.print(p[2]);
      lcd.print(" СЕК");
      lcd.setCursor(4, 3);
      lcd.print(" ");
      lcd.print(p[4]);
      lcd.print(" ");
      lcd.print(p[5]);
      lcd.print(" ");
      lcd.print(p[6]);
      lcd.print(" ");
      lcd.print(p[7]);
      lcd.print(" ");
      lcd.print(p[8]);
      lcd.write(126);
      lcd.print(p[9]);
      lcd.print(" ");
      lcd.print(p[3]);
      if (enc1.isRightH()) {
        //  lcd.clear();
        if (++p[9] >= 2) p[9] = 0;
      } else if (enc1.isLeftH()) {
        //   lcd.clear();
        if (--p[9] < 0) p[9] = 1;
      }

      break;
    case 9:
      lcd.setCursor(5, 1);
      lcd.print(" ");
      lcd.setCursor(6, 1);
      if (p[0] < 10) {
        lcd.print("0");
      }
      lcd.print(p[0]);
      lcd.print(":");
      if (p[1] < 10) {
        lcd.print("0");
      }
      lcd.print(p[1]);
      lcd.setCursor(13, 1);
      lcd.print(" ");
      lcd.setCursor(14, 1);
      lcd.print(p[2]);
      lcd.print(" СЕК");
      lcd.setCursor(4, 3);
      lcd.print(" ");
      lcd.print(p[4]);
      lcd.print(" ");
      lcd.print(p[5]);
      lcd.print(" ");
      lcd.print(p[6]);
      lcd.print(" ");
      lcd.print(p[7]);
      lcd.print(" ");
      lcd.print(p[8]);
      lcd.print(" ");
      lcd.print(p[9]);
      lcd.write(126);
      lcd.print(p[3]);
      if (enc1.isRightH()) {
        //  lcd.clear();
        if (++p[3] >= 2) p[3] = 0;
      } else if (enc1.isLeftH()) {
        //   lcd.clear();
        if (--p[3] < 0) p[3] = 1;
      }

      break;
  }



}
